"""
URL configuration for bdd_med project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.decorators.cache import cache_page
from django.views.i18n import JavaScriptCatalog

from rest_framework import routers

from .admin import admin_site

import media.views
import users.views

# API router
router = routers.DefaultRouter()
router.register(r'media/author', media.views.AuthorViewSet)
router.register(r'media/borrowable', media.views.BorrowablePolymorphicViewSet)
router.register(r'media/comic', media.views.ComicViewSet)
router.register(r'media/manga', media.views.MangaViewSet)
router.register(r'media/novel', media.views.NovelViewSet)
router.register(r'media/vinyl', media.views.VinylViewSet)
router.register(r'media/cd', media.views.CDViewSet)
router.register(r'media/track', media.views.TrackViewSet)
router.register(r'media/review', media.views.ReviewViewSet)
router.register(r'media/game', media.views.GameViewSet)
router.register(r'media/borrow', media.views.BorrowViewSet)
router.register(r'users/user', users.views.UserViewSet)
router.register(r'users/group', users.views.GroupViewSet)


urlpatterns = [
    path('', users.views.IndexView.as_view(), name='index'),
    path('admin/', admin_site.urls, name="admin"),

    path('users/', include('users.urls')),

    # Include Django Contrib and Core routers
    path('accounts/', include('django.contrib.auth.urls')),
    path('media/', include('media.urls')),

    # REST API
    path('api/', include(router.urls)),

    # JS translation
    path(
        'jsi18n/',
        cache_page(3600)(JavaScriptCatalog.as_view()),
        name='javascript-catalog'
    ),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
