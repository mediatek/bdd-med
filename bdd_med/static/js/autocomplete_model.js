window.addEventListener("load", (event) => {
    let tab = Array.from(document.getElementsByClassName('autocomplete'))
    tab.forEach(function (target) {
        const prefix = target.id
        const api_url = target.getAttribute("api_url")
        let api_url_suffix = target.getAttribute("api_url_suffix")
        if (!api_url_suffix) { api_url_suffix = '' }
        let name_field = target.getAttribute("name_field")
        if (!name_field) { name_field = 'name' }
        const tooltip = document.getElementById(prefix + "_tooltip")

        // When the user clicks elsewhere, we hide the tooltip
        document.addEventListener("click", function (e) {
            if (!e.target.id.startsWith(prefix)) {
                tooltip.hidden = true
            }
        })

        let old_pattern = null
        let results = []

        // Clear search on click
        target.addEventListener('click', function (e) {
            tooltip.hidden = true
            target.classList.remove('is-invalid')
            target.value = ''
            old_pattern = ''
        })

        // // When the user type "Enter", the first alias is clicked
        target.addEventListener('keydown', function (e) {
            if (e.key === "Enter") {
                e.preventDefault()
                if (results.length > 0) {
                    document.getElementById(prefix + '_' + results[0]).click()
                }
            }
        })

        // When the user type something, the matched aliases are refreshed
        target.addEventListener('input', function (e) {
            target.classList.add('is-invalid')

            const pattern = target.value

            // If the pattern is not modified, we don't query the API
            if (pattern === old_pattern) { return }
            old_pattern = pattern
            results.length = 0

            // get matched Alias with note associated
            if (pattern === '') {
                tooltip.hidden = true
                return
            }

            const api_request = new XMLHttpRequest()
            api_request.open('GET', api_url + (api_url.includes('?') ? '&' : '?') + 'format=json&search=' + pattern + api_url_suffix)
            api_request.onload = function () {
                // The response arrived too late, we stop the request
                if (pattern !== target.value) { return }

                let objects = JSON.parse(api_request.responseText)
                let html = '<ul class="list-group list-group-flush" id="' + prefix + '_list">'
                objects.results.forEach(function (obj) {
                    html += "<li id=" + prefix + '_' + obj.id + ">" + obj[name_field] + "</li>"
                    results.push(obj.id)
                })
                html += '</ul>'

                tooltip.hidden = false
                tooltip.innerHTML = html

                objects.results.forEach(function (obj) {
                    const line = document.getElementById(prefix + '_' + obj.id)
                    line.addEventListener('click', function (e) {
                        target.value = obj[name_field]
                        document.getElementById(prefix + '_pk').value = obj.id

                        tooltip.hidden = true
                        target.classList.remove('is-invalid')
                        target.classList.add('is-valid')

                        if (typeof autocompleted !== 'undefined') { autocompleted(obj, prefix) }
                    })
                })
            }
            api_request.send()
        })
        for (target in Array.from(document.getElementsByClassName('autocomplete-reset'))) {
            target.addEventListener('click', function (e) {
                const prefix = target.id.replace('_reset', '')
                document.getElementById$(prefix + '_pk').value = ''
                document.getElementById(prefix).value = ''
                document.getElementById(prefix + '_tooltip').hidden = true
            })
        }
    })
})
