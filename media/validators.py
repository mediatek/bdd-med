"""
Based on https://github.com/secnot/django-isbn-field
"""

from django.core.exceptions import ValidationError


def isbn_validator(raw_isbn):
    """Check string is a valid ISBN number"""
    isbn_to_check = raw_isbn.replace('-', '').replace(' ', '')

    if not isinstance(isbn_to_check, str):
        raise ValidationError('Invalid ISBN: Not a string')

    if len(isbn_to_check) != 10 and len(isbn_to_check) != 13:
        raise ValidationError('Invalid ISBN: Wrong length')

    # if not isbn.is_valid(isbn_to_check):
    #    raise ValidationError(_('Invalid ISBN: Failed checksum'))

    if isbn_to_check != isbn_to_check.upper():
        raise ValidationError('Invalid ISBN: Only upper case allowed')

    return True
