from django_tables2 import tables, LinkColumn, A, ManyToManyColumn, Column
from django.utils.html import mark_safe

from .models import Borrow, Borrowable, Book, Disk, Game, Review


class BorrowablePolymorphicTable(tables.Table):
    title = LinkColumn('media:detail', args=(A('pk'), ))

    polymorphic_ctype = Column(verbose_name="Type")

    def render_polymorphic_ctype(self, value):
        return value.name

    class Meta:
        model = Borrowable
        exclude = ('id', 'amount')
        sequence = ('title', 'subtitle', '...')


class BookTable(tables.Table):
    title = LinkColumn('media:detail', args=[A('pk'), ])

    authors = ManyToManyColumn(transform=lambda author: author.name)

    class Meta:
        model = Book
        exclude = ('id', 'borrowable_ptr', 'medium_ptr', 'polymorphic_ctype')
        sequence = ('title', 'subtitle', 'authors', '...', 'owner', 'comment')


class DiskTable(tables.Table):
    title = LinkColumn('media:detail', args=[A('pk'), ])

    authors = ManyToManyColumn(transform=lambda author: author.name)

    class Meta:
        model = Disk
        exclude = ('id', 'polymorphic_ctype', 'medium_ptr')
        sequence = ('title', 'subtitle', 'authors', '...', 'owner', 'comment')


class GameTable(tables.Table):
    title = LinkColumn(
        'media:detail',
        args=[A('pk'), ],
    )

    players = Column(verbose_name="Nombre de joueureuses", accessor="pk")

    def render_players(self, value, record):
        nmin, nmax = record.players_min, record.players_max
        if nmin == nmax:
            return nmin
        else:
            return f"{nmin} à {nmax}"

    class Meta:
        model = Game
        exclude = ('id', 'borrowable_ptr', 'polymorphic_ctype', 'players_min',
                   'players_max')
        sequence = ('title', 'subtitle', '...', 'owner', 'comment')


class ReviewTable(tables.Table):
    title = LinkColumn(
        'media:detail',
        args=[A('pk'), ],
    )

    class Meta:
        model = Review
        exclude = ('id', 'borrowable_ptr', 'polymorphic_ctype')
        sequence = ('title', 'subtitle', '...', 'owner', 'comment')


class MyLoansTable(tables.Table):
    class Meta:
        model = Borrow
        exclude = ('id',)


class ReturnTable(tables.Table):
    class Meta:
        model = Borrow
        fields = ('user', 'borrowable', 'borrow_date')
        sequence = ("select", "...")

    select = Column(
        verbose_name="",
        orderable=False,
        accessor="pk",
        attrs={
            "td": {
                "style":
                "white-space:nowrap;width:1%;padding-right:0px;padding-left:8px"
            }
        }
    )

    def render_select(self, value):
        val = '<input type="checkbox" name="give_back" value=' + str(value) +\
              ' id=id_select_' + str(value) + '>'
        return mark_safe(val)


class HistoryTable(tables.Table):
    class Meta:
        model = Borrow
        exclude = ('id',)
