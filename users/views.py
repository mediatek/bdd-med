from authlib.integrations.django_client import OAuth
from django.contrib.auth import login, logout
from django.contrib.auth.models import Group
from django.urls import reverse
from django.utils import timezone
from django.views.generic import RedirectView, TemplateView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from users.models import User, AccessToken

from .serializers import GroupSerializer, UserSerializer


class IndexView(TemplateView):
    """
    Test
    """
    template_name = 'users/index.html'


class LoginView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        oauth = OAuth()
        oauth.register('notekfet')
        redirect_url = self.request.build_absolute_uri(reverse('users:auth'))
        return oauth.notekfet.authorize_redirect(self.request,
                                                 redirect_url).url


class LogoutView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        logout(self.request)
        return reverse('index')


class AuthorizeView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        oauth = OAuth()
        oauth.register('notekfet')
        token = oauth.notekfet.authorize_access_token(self.request)
        token_obj = AccessToken.objects.create(
            access_token=token['access_token'],
            expires_in=token['expires_in'],
            scopes=token['scope'],
            refresh_token=token['refresh_token'],
            expires_at=timezone.now() + timezone.timedelta(
                seconds=token['expires_in']),
        )
        user = token_obj.fetch_user(True)
        self.request.session['access_token_id'] = token_obj.id
        self.request.session.save()
        login(self.request, user)
        return reverse('index')


class UserViewSet(ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ('username', 'first_name', 'last_name', 'email',
                        'groups', 'date_joined')
    search_fields = ['username', 'first_name', 'last_name']


class GroupViewSet(ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
