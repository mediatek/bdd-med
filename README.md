# Base de données de la Médiatek

Ce projet permet la gestion de la base de données de la médiathèque de l'ENS Paris-Saclay.
Cette dernière sert à référencer les médias (BD, romans, mangas, jeux, ...) et à
gérer les emprunts des adhérents.

## Licence

Ce projet est sous la licence GNU public license v3.0

## Installation

### Instance de développement

On peut développer soit avec Docker, soit en utilisant un VirtuelEnv.

Dans le cas du VirtualEnv,

```bash
python -m venv env
. env/bin/activate
pip install -r requirements.txt
./manage.py makemigrations
./manage.py migrate
./manage.py runserver
```

Il faut ensuite copier le fichier `.env_example` en `.env` et changer ce qui
convient.

## Interface avec la Note Kfet

La connection se fait au moyen de la ![note kfet](https://gitlab.crans.org/bde/nk20).
Pour cela, il faut créet une application OAuth2. Noter le client id et le client
secret, et choisir Authorization code comme Authorization grant type. Dans
redirect uris, renseigner `adresse du site/users/authorize`.

Ensuite, dans le fichier `.env`, renseigner l'url de la note (localhost,
note-dev ou note), et l'id et le secret précédents.
