window.addEventListener("load", (event) => {
    let tab = Array.from(document.getElementsByClassName('autocompletemultiple'))
    tab.forEach(function (target) {
        const prefix = target.id
        const api_url = target.getAttribute("api_url")
        let api_url_suffix = target.getAttribute("api_url_suffix")
        if (!api_url_suffix) { api_url_suffix = '' }
        let name_field = target.getAttribute("name_field")
        if (!name_field) { name_field = 'name' }
        const tooltip = document.getElementById(prefix + "_tooltip")
        const display = document.getElementById(prefix + "_display")
        const initial = document.getElementById(prefix + "_initial")

        // Configure tooltip
        // target.tooltip({
        //     html: true,
        //     placement: 'bottom',
        //     title: 'Loading...',
        //     trigger: 'manual',
        //     container: target.parent(),
        //     fallbackPlacement: 'clockwise'
        // })

        // When the user clicks elsewhere, we hide the tooltip
        document.addEventListener("click", function (e) {
            if (!e.target.id.startsWith(prefix)) {
                tooltip.hidden = true
            }
        })

        let old_pattern = null

        let results = []
        let selected = []

        function display_selected () {
            let html = ''
            selected.forEach(function (disp) {
                html += "<li id=" + prefix + '_selected_' + disp.id + ">" +
                    disp.name +
                    '<input type=hidden name="' + target.getAttribute("widget_name") +
                    '" value=' + disp.id + '></li>'
            })

            // Selected items are displayed
            display.innerHTML = html

            selected.forEach(function (disp) {
                const line_obj = document.getElementById(prefix + '_selected_' + disp.id)
                // When a result is clicked, it is removed
                line_obj.addEventListener('click', function (e) {
                    const new_selected = []
                    let html = ''
                    selected.forEach(function (d) {
                        if (disp.id !== d.id) {
                            new_selected.push(d)
                        }
                    })

                    selected = new_selected
                    display_selected()
                })
            })
        }

        Array.from(initial.children).forEach(function (element) {
            selected.push({
                name: element.getAttribute("value_name"),
                id: element.getAttribute("value_pk")
            })
        })

        display_selected()

        // Clear search on click
        target.addEventListener('click', function (e) {
            tooltip.hidden = true
            target.classList.remove('is-invalid')
            target.value = ''
            old_pattern = ''
            results.length = 0
        })

        // When the user type "Enter", the first alias is clicked
        target.addEventListener('keydown', function (e) {
            if (e.key === "Enter") {
                e.preventDefault()
                if (results.length > 0) {
                    document.getElementById(prefix + '_' + results[0]).click()
                }
            }
        })

        // When the user type something, the matched aliases are refreshed
        target.addEventListener('input', function (e) {
            target.classList.add('is-invalid')

            const pattern = target.value

            // If the pattern is not modified, we don't query the API
            if (pattern === old_pattern) { return }
            old_pattern = pattern
            results.length = 0

            // get matched Alias with note associated
            if (pattern === '') {
                tooltip.hidden = true
                return
            }

            const api_request = new XMLHttpRequest()
            api_request.open('GET', api_url + (api_url.includes('?') ? '&' : '?') + 'format=json&search=' + pattern + api_url_suffix)
            api_request.onload = function () {
                // The response arrived too late, we stop the request
                if (pattern !== target.value) { return }

                let objects = JSON.parse(api_request.responseText)
                let html = '<ul class="list-group list-group-flush" id="' + prefix + '_list">'
                objects.results.forEach(function (obj) {
                    html += "<li id=" + prefix + '_' + obj.id + ">" + obj[name_field] + "</li>"
                    results.push(obj.id)
                })
                html += '</ul>'

                tooltip.hidden = false
                tooltip.innerHTML = html

                objects.results.forEach(function (obj) {
                    const line = document.getElementById(prefix + '_' + obj.id)
                    line.addEventListener('click', function (e) {
                        selected.forEach(function (d) {
                                // If result is already selected, do nothing
                                if (d.id === obj.id) {
                                    return
                                }
                            })
                        // In the other case, we add a new result
                        disp = {
                            name: obj[name_field],
                            id: obj.id,
                        }
                        selected.push(disp)

                        display_selected()

                        target.value = ''
                        old_pattern = ''
                        results.length = 0
                        tooltip.hidden = true
                        target.classList.remove('is-invalid')
                        target.classList.add('is-valid')
                    })
                })
            }
            api_request.send()
        })

    })
})
