from django.contrib import admin
from bdd_med.admin import admin_site

from .models import Author, Comic, Manga, Novel, Vinyl, CD, Borrow, Track, \
    Review, Game


@admin.register(Author, site=admin_site)
class AuthorAdmin(admin.ModelAdmin):
    pass


class MediumAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'authors_list', 'side_identifier', 'isbn',
                    'present')
    search_fields = ('title', 'subtitle', 'authors__name', 'side_identifier',
                     'isbn')
    # autocomplete_fields = ('authors')

    def authors_list(self, obj):
        return ", ".join([a.name for a in obj.authors.all()])
    authors_list.short_description = "auteurices"


class TrackInLines(admin.TabularInline):
    model = Track


class DiskAdmin(MediumAdmin):
    inlines = (TrackInLines,)


@admin.register(Review, site=admin_site)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'year', 'month', 'day')
    search_fields = ('title', 'number', 'year')


@admin.register(Game, site=admin_site)
class GameAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'duration', 'players_min', 'players_max',
                    'isbn')
    search_fields = ('title', 'subtitle', 'isbn', 'duration')


@admin.register(Borrow, site=admin_site)
class BorrowAdmin(admin.ModelAdmin):
    list_display = ('borrowable', 'user', 'borrow_date', 'given_back')
    search_fields = ('borrowable__isbn', 'borrowable__title',
                     'borrowable__medium__side_identifier', 'user__username',
                     'borrow_date', 'given_back')
    date_hierarchy = 'borrow_date'


admin_site.register(Comic, MediumAdmin)
admin_site.register(Manga, MediumAdmin)
admin_site.register(Novel, MediumAdmin)
admin_site.register(Vinyl, DiskAdmin)
admin_site.register(CD, DiskAdmin)
