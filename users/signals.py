def add_to_default_group(sender, **kwargs):
    """
    When creating a new user, add it to users group
    """
    user = kwargs["instance"]
    if kwargs["created"]:
        from django.contrib.auth.models import Group
        group, group_created = Group.objects.get_or_create(name='users')
        user.groups.add(group)
