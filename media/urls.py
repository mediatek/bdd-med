from django.urls import path

from . import views

app_name = 'media'
urlpatterns = [
    path('borrow/', views.NewBorrowFormView.as_view(), name="borrow"),
    path('return/', views.ReturnView.as_view(), name="return"),

    path('<int:pk>/', views.MediaDetailView.as_view(), name='detail'),
    path('add/', views.CreateBorrowableView.as_view(), name="add"),
    path('edit/<int:pk>', views.EditBorrowableView.as_view(), name="edit"),

    path('find/', views.PolymorphicListView.as_view(), name='find'),
    path('find-advanced/', views.AdvancedSearchView.as_view(),
         name='find-advanced'),

    path('mes_emprunts/', views.MyLoansListView.as_view(),
         name="mes_emprunts"),
    path('return/', views.ReturnView.as_view(), name='return'),
]
