from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from bdd_med.admin import admin_site

from .models import User


@admin.register(User, site=admin_site)
class CustomUserAdmin(UserAdmin):
    """
    Admin customisation for User
    """
    list_display = ('username', 'first_name', 'last_name', 'email', 'is_active', 'is_staff')
