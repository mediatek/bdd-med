from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from .models import Author, Borrowable, Comic, Manga, Novel, Vinyl, CD, Game, \
    Borrow, Track, Review


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'


class BorrowableSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = Borrowable
        fields = '__all__'


class ComicSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = Comic
        fields = '__all__'


class MangaSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = Manga
        fields = '__all__'


class NovelSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = Novel
        fields = '__all__'


class VinylSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = Vinyl
        fields = '__all__'


class CDSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = CD
        fields = '__all__'


class TrackSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = Track
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = Review
        fields = '__all__'


class GameSerializer(serializers.ModelSerializer):
    string_repr = serializers.SerializerMethodField()

    def get_string_repr(self, obj):
        return str(obj)

    class Meta:
        model = Game
        fields = '__all__'


class BorrowablePolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Borrowable: BorrowableSerializer,
        Comic: ComicSerializer,
        Manga: MangaSerializer,
        Novel: NovelSerializer,
        Vinyl: VinylSerializer,
        CD: CDSerializer,
        Review: ReviewSerializer,
        Game: GameSerializer,
    }

    class Meta:
        model = Borrowable


class BorrowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Borrow
        fields = '__all__'
