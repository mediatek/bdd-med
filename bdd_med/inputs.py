
from django.forms.widgets import TextInput


class Autocomplete(TextInput):
    """
    Autocomplete widget for a foreign key
    """

    template_name = "autocomplete_model.html"

    def __init__(self, model, resetable=False, attrs=None):
        super().__init__(attrs | {'class': 'autocomplete'})

        self.model = model
        self.resetable = resetable

    class Media:
        js = ('js/autocomplete_model.js', )

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['resetable'] = self.resetable
        context['widget']['model_pk'] = value
        return context

    def format_value(self, value):
        if value:
            return str(self.model.objects.get(pk=int(value)))
        return ""


class AutocompleteMultiple(TextInput):
    """
    Autocomplete widget for a many to many
    """

    template_name = "autocomplete_multiple_model.html"

    def __init__(self, model, title=None, attrs=None):
        super().__init__(attrs | {'class': 'autocompletemultiple'})

        self.model = model
        self.title = title

    class Media:
        js = ('js/autocomplete_multiple_model.js', )

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['title'] = self.title
        context['widget']['values'] = \
            zip(value, context['widget']['value']) \
            if value else []
        return context

    def value_from_datadict(self, data, files, name):
        try:
            getter = data.getlist
        except AttributeError:
            getter = data.get
        return getter(name)

    def format_value(self, value):
        if value is None:
            return []
        if not isinstance(value, (tuple, list)):
            value = [str(self.model.objects.get(pk=int(value)))]
        return [str(self.model.objects.get(pk=int(v)))
                if v is not None else "" for v in value]
