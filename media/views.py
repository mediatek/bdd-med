import re
import unicodedata

from django.utils import timezone
from django.db import transaction
from django.db.models import Q, QuerySet
from django.forms.models import construct_instance
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse, \
    HttpResponseBadRequest
from django.views.generic import DetailView, FormView
from django.contrib.contenttypes.models import ContentType
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from django_tables2 import SingleTableView
from formset.views import FormCollectionView, EditCollectionView

from . import serializers
from . import tables
from . import forms
from .models import Author, Borrowable, Medium, Book, Novel, Comic, Manga, \
    Disk, CD, Vinyl, Review, Game, Track, Borrow


def generate_side_identifier(title, authors, subtitle=None):
    if isinstance(authors, QuerySet):
        authors = list(authors)

    title_normalized = title.upper()
    title_normalized = title_normalized.replace('’', '\'')
    title_normalized = re.sub(r'^DE ', '', title_normalized)
    title_normalized = re.sub(r'^DES ', '', title_normalized)
    title_normalized = re.sub(r'^LE ', '', title_normalized)
    title_normalized = re.sub(r'^LA ', '', title_normalized)
    title_normalized = re.sub(r'^LES ', '', title_normalized)
    title_normalized = re.sub(r'^L\'', '', title_normalized)
    title_normalized = re.sub(r'^UN ', '', title_normalized)
    title_normalized = re.sub(r'^UNE ', '', title_normalized)
    title_normalized = re.sub(r'^THE ', '', title_normalized)
    title_normalized = re.sub(r'Œ', 'OE', title_normalized)
    title_normalized = title_normalized.replace(' ', '')
    title_normalized = ''.join(
        char
        for char in unicodedata.normalize(
            'NFKD', title_normalized.casefold())
        if all(not unicodedata.category(char).startswith(cat)
               for cat in {'M', 'P', 'Z', 'C'}) or char == ' '
    ).casefold().upper()
    title_normalized = re.sub("[^A-Z0-9$]", "", title_normalized)
    authors = authors.copy()

    def sort(author):
        return author.name.split(" ")[-1] + ".{:042d}".format(author.pk)

    authors.sort(key=sort)
    primary_author = authors[0]
    author_name = primary_author.name.upper()
    if ',' not in author_name and ' ' in author_name:
        author_name = author_name.split(' ')[-1]
    author_name = ''.join(
        char for char in unicodedata.normalize('NFKD', author_name.casefold())
        if all(not unicodedata.category(char).startswith(cat)
               for cat in {'M', 'P', 'Z', 'C'}) or char == ' '
    ).casefold().upper()
    author_name = re.sub("[^A-Z]", "", author_name)
    side_identifier = "{:.3} {:.3}".format(author_name, title_normalized, )
    if subtitle:
        subtitle = re.sub(r'</span>', '', subtitle)
        subtitle = re.sub(r'<span.*>', '', subtitle)
        start = subtitle.split(' ')[0].replace('.', '')
        start = re.sub("^R?", "", start)

        if start.isnumeric():
            side_identifier += " {:0>2}".format(start, )

    # Normalize side identifier, in order to remove accents
    side_identifier = ''.join(
        char for char in unicodedata.normalize('NFKD',
                                               side_identifier.casefold())
        if all(not unicodedata.category(char).startswith(cat)
               for cat in {'M', 'P', 'Z', 'C'})
        or char == ' ').casefold().upper()

    return side_identifier


class SideIdentifierMixin:
    def form_collection_valid(self, form_collection):
        if form_collection.partial and "borrowable" in \
                form_collection.valid_holders:
            valid_holder = form_collection.valid_holders.get("borrowable")
            if not valid_holder:
                return HttpResponseBadRequest("Form data is missing.")
            identifier = generate_side_identifier(
                valid_holder.cleaned_data.get("title"),
                valid_holder.cleaned_data.get("authors"),
                valid_holder.cleaned_data.get("subtitle"))
            return JsonResponse({"identifier": identifier})
        return super().form_collection_valid(form_collection)


class NewAuthorMixin:
    def form_collection_valid(self, form_collection):
        if form_collection.partial and 'add_author' in \
                form_collection.valid_holders:
            valid_holder = form_collection.valid_holders.get('add_author')
            if not valid_holder:
                return HttpResponseBadRequest("Form data is missing.")
            author = construct_instance(valid_holder, Author())
            author.save()
            return JsonResponse({"author_id": author.id})
        return super().form_collection_valid(form_collection)


# TODO permissions
class NewBorrowFormView(FormView):
    """
    Form to Borrow a Borrowable
    """
    template_name = "media/borrow_form.html"
    form_class = forms.BorrowForm
    extra_context = {'title': "Emprunter un média"}

    def form_valid(self, form):
        data = form.cleaned_data
        user = data["user"]
        items_pk = data["borrowed_items"]
        for item in items_pk:
            new_borrow = Borrow()
            new_borrow.user = user
            new_borrow.borrowable = item
            new_borrow.borrow_date = timezone.now()
            new_borrow.borrowed_with = self.request.user
            new_borrow.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse("media:borrow")


# TODO permissions
class CreateBorrowableView(SideIdentifierMixin, NewAuthorMixin,
                           FormCollectionView):
    collection_class = forms.BorrowableCollection
    template_name = "media/borrowable_form.html"
    extra_context = {'title': "Nouveau média"}

    def get_success_url(self):
        return reverse('media:edit', kwargs={"pk": self.object.pk})

    def form_collection_valid(self, form_collection):
        if not form_collection.partial:
            with transaction.atomic():
                self.object = form_collection.construct_instance()
            if not form_collection.is_valid():
                return self.form_collection_invalid(form_collection)
        return super().form_collection_valid(form_collection)


# TODO permissions
class EditBorrowableView(SideIdentifierMixin, NewAuthorMixin,
                         EditCollectionView):
    template_name = "media/edit.html"
    model = Borrowable

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['title'] = f"Édition - {self.object} \
                ({type(self.object)._meta.verbose_name})"
        return data

    def get_collection_class(self):
        return forms.borrowableform_factory(type(self.object))

    def get_success_url(self):
        data = self.get_extra_data()
        if data and data.get('delete'):
            return reverse("media:add")
        return reverse("media:detail", args=[self.kwargs.get('pk')])

    def form_collection_valid(self, form_collection):
        data = self.get_extra_data()
        if data and data.get('delete'):
            form_collection.instance.delete()
            return JsonResponse({'success_url': self.get_success_url()})
        return super().form_collection_valid(form_collection)


# TODO permissions
class ReturnView(SingleTableView):
    """
    Form to return a Borrowable
    """
    model = Borrow
    table_class = tables.ReturnTable
    template_name = "media/return.html"
    extra_context = {'title': "Retour d'emprunts"}

    def get_queryset(self, **kwargs):
        qs = super().get_queryset(**kwargs).distinct()
        return qs.filter(given_back__isnull=True).order_by('borrow_date')

    def post(self, request, *args, **kwargs):
        data = request.POST
        to_give_back = data.getlist("give_back")
        for id in to_give_back:
            borrow = Borrow.objects.get(pk=int(id))
            borrow.given_back = timezone.now()
            borrow.given_back_to = request.user
            borrow.save()

        return HttpResponseRedirect(reverse("media:return"))


# TODO permissions ?
class PolymorphicListView(SingleTableView):
    model = Borrowable
    table_class = tables.BorrowablePolymorphicTable
    template_name = "borrowable_list.html"
    extra_context = {'title': "Recherche de média"}

    def filter_queryset(self, pattern):
        try:
            int(pattern)
            isbn_q = Q(isbn=pattern)
        except ValueError:
            isbn_q = Q()
        return Q(title__icontains=pattern) | Q(subtitle__icontains=pattern) | \
            Q(medium__side_identifier__icontains=pattern) | \
            Q(medium__authors__name__icontains=pattern) | isbn_q

    def get_queryset(self):
        """
        Filter the queryset according to the pattern
        """
        qs = super().get_queryset()
        if "search" in self.request.GET:
            pattern = self.request.GET["search"]
            qs = qs.filter(self.filter_queryset(pattern)
                           ).order_by('title', 'isbn')
        return qs.distinct()


# TODO permissions ?
class AdvancedSearchView(SingleTableView):
    template_name = "media/advanced_search.html"
    extra_context = {'title': "Recherche avancée"}

    @property
    def form(self):
        if hasattr(self, "_form"):
            return self._form
        else:
            form = forms.SearchBorrowableForm(data=self.request.GET if
                                              self.request.GET else None)
            self._form = form
            return form

    @property
    def cleaned_data(self):
        self.form.full_clean()
        return getattr(self.form, "cleaned_data", {})

    def get_table_class(self):
        data = self.cleaned_data
        if self.form.is_valid():
            return {'Manga': tables.BookTable, 'BD': tables.BookTable, 'Roman':
                    tables.BookTable, 'CD': tables.DiskTable, 'Vinyle':
                    tables.DiskTable, 'Jeu': tables.GameTable, 'Magazine':
                    tables.ReviewTable
                    }.get(data["medium_type"])
        else:
            return None

    def get_table(self, **kwargs):
        if self.get_table_class() is None:
            return None
        else:
            return super().get_table(**kwargs)

    # flake8: noqa: C901
    def get_queryset(self):
        data = self.cleaned_data
        if not self.form.is_valid():
            return Borrowable.objects.none()

        model = {'Manga': Manga, 'BD': Comic, 'Roman': Novel, 'CD': CD,
                 'Vinyle': Vinyl, 'Jeu': Game, 'Magazine':
                 Review}.get(data["medium_type"])
        q_object = \
            Q(polymorphic_ctype=ContentType.objects.get_for_model(model))
        if data.get("title"):
            q_object &= Q(title__icontains=data["title"])
        if data.get("subtitle"):
            q_object &= Q(subtitle__icontains=data["subtitle"])
        if issubclass(model, Medium) and data.get("authors"):
            q_object &= Q(authors__name__icontains=data["authors"])
        if data.get("subtitle"):
            q_object &= Q(subtitle__icontains=data["subtitle"])
        if data.get("owner"):
            q_object &= Q(owner__icontains=data["owner"])
        if data.get("isbn"):
            q_object &= Q(isbn__istartswith=data["isbn"])
        if issubclass(model, Medium) and data.get("side_identifier"):
            q_object &= Q(side_identifier__icontains=data["side_identifier"])
        if issubclass(model, Book) and data.get("publish_date_gte"):
            q_object &= Q(publish_date__gte=data["publish_date_gte"])
        if issubclass(model, Book) and data.get("publish_date_lte"):
            q_object &= Q(publish_date__lte=data["publish_date_lte"])
        if issubclass(model, Disk) and data.get("publish_date_lte"):
            q_object &= Q(tracklist__name__icontains=data["track"])
        if issubclass(model, Review):
            if data.get("number"):
                q_object &= Q(number=data["number"])
            if data.get("year"):
                q_object &= Q(year=data["year"])
            if data.get("month"):
                q_object &= Q(month=data["month"])
            if data.get("day"):
                q_object &= Q(day=data["day"])
        if issubclass(model, Game):
            if data.get("duration"):
                q_object &= Q(duration=data["duration"])
            if data.get("players"):
                q_object &= Q(players_min__lte=data["players"],
                              players_max__gte=data["players"])
        if data.get("comment"):
            q_object &= Q(comment__icontains=data["comment"])
        return model.objects.filter(q_object)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = self.form
        return context


# TODO permissions
class MyLoansListView(SingleTableView):
    """
    Display a list of all loans
    """
    model = Borrow
    table_class = tables.MyLoansTable
    extra_context = {'title': "Mes emprunts"}

    def get_queryset(self, **kwargs):
        """
        Restrict to user loans
        """
        return Borrow.objects.filter(
            user=self.request.user
        )


# TODO permissions ?
class MediaDetailView(DetailView):
    """
    Shows details about one book
    """
    model = Borrowable
    context_object_name = "media"

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['title'] = f"Détail - {self.object} \
                ({type(self.object)._meta.verbose_name})"
        return data


class AuthorViewSet(ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = serializers.AuthorSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['name', ]
    search_fields = ['name', ]


class BorrowablePolymorphicViewSet(ModelViewSet):
    queryset = Borrowable.objects.order_by("id")
    serializer_class = serializers.BorrowablePolymorphicSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['present', 'isbn', 'owner']
    search_fields = ['=isbn', 'title', 'subtitle', 'medium__side_identifier',
                     'medium__authors__name']


class ComicViewSet(ModelViewSet):
    queryset = Comic.objects.all()
    serializer_class = serializers.ComicSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['present', 'isbn', 'owner']
    search_fields = ['=isbn', 'title', 'subtitle', 'side_identifier',
                     'authors__name', 'publish_date']


class MangaViewSet(ModelViewSet):
    queryset = Manga.objects.all()
    serializer_class = serializers.MangaSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['present', 'isbn', 'owner']
    search_fields = ['=isbn', 'title', 'subtitle', 'side_identifier',
                     'authors__name', 'publish_date']


class NovelViewSet(ModelViewSet):
    queryset = Novel.objects.all()
    serializer_class = serializers.NovelSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['present', 'isbn', 'owner']
    search_fields = ['=isbn', 'title', 'subtitle', 'side_identifier',
                     'authors__name', 'publish_date']


class VinylViewSet(ModelViewSet):
    queryset = Vinyl.objects.all()
    serializer_class = serializers.VinylSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['present', 'isbn', 'owner']
    search_fields = ['title', 'subtitle', 'side_identifier',
                     'authors__name', 'publish_date', 'tracklist__name']


class CDViewSet(ModelViewSet):
    queryset = CD.objects.all()
    serializer_class = serializers.CDSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['present', 'isbn', 'owner']
    search_fields = ['title', 'subtitle', 'side_identifier',
                     'authors__name', 'publish_date', 'tracklist__name']


class TrackViewSet(ModelViewSet):
    queryset = Track.objects.all()
    serializer_class = serializers.TrackSerializer


class ReviewViewSet(ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = serializers.ReviewSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['present', 'isbn', 'owner', 'number', 'year', 'month',
                        'day']
    search_fields = ['title', 'subtitle']


class GameViewSet(ModelViewSet):
    queryset = Game.objects.all()
    serializer_class = serializers.GameSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ('present', 'isbn', 'owner', 'duration', 'players_min',
                        'players_max')
    search_fields = ['title', 'subtitle']


class BorrowViewSet(ModelViewSet):
    queryset = Borrow.objects.all()
    serializer_class = serializers.BorrowSerializer
