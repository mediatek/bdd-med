from django.conf import settings
from django.db import models
from django.db.models import Q
from django.core.validators import MinValueValidator
from django.utils import timezone
from polymorphic.models import PolymorphicModel

from .fields import ISBNField


class Author(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name="nom"
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "auteurice"
        verbose_name_plural = "auteurices"


class Borrowable(PolymorphicModel):
    present = models.BooleanField(
        verbose_name="présent",
        default=False
    )

    title = models.CharField(
        max_length=255,
        verbose_name="titre"
    )

    subtitle = models.CharField(
        max_length=255,
        verbose_name="sous-titre",
        blank=True
    )

    owner = models.CharField(
        max_length=255,
        verbose_name="propriétaire"
    )

    amount = models.IntegerField(
        default=1,
        verbose_name="quantité",
    )

    isbn = ISBNField(
        "ISBN",
        unique=True,
        blank=True,
        null=True
    )

    comment = models.CharField(
        max_length=255,
        verbose_name="commentaire",
        blank=True
    )

    def __str__(self):
        text = self.title
        if self.subtitle:
            text = f"{text} : {self.subtitle}"
        return text

    def update_present_status(self):
        borrowed = Borrow.objects.filter(
            Q(borrowable=self)
            & (Q(given_back__isnull=True)
               | Q(given_back__gte=timezone.now()))).exists()
        self.present = not borrowed
        self.save()

    class Meta:
        verbose_name = "empruntable"
        verbose_name_plural = "empruntables"


class Medium(Borrowable):
    side_identifier = models.CharField(
        max_length=255,
        verbose_name="cote"
    )

    authors = models.ManyToManyField(
        Author,
        verbose_name="auteurices",
        blank=True
    )

    def authors_str(self):
        return ", ".join([s.name for s in self.authors.all()])

    class Meta:
        verbose_name = "média"
        verbose_name_plural = "médias"


class Book(Medium):
    publish_date = models.DateField(
        verbose_name="date de parution",
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = "livre"
        verbose_name_plural = "livres"


class Comic(Book):
    class Meta:
        verbose_name = "BD"
        verbose_name_plural = "BDs"
        ordering = ['title', 'subtitle']


class Manga(Book):
    class Meta:
        verbose_name = "manga"
        verbose_name_plural = "mangas"
        ordering = ['title', 'subtitle']


class Novel(Book):
    class Meta:
        verbose_name = "roman"
        verbose_name_plural = "romans"
        ordering = ['title', 'subtitle']


class Disk(Medium):
    class Meta:
        verbose_name = "disque"
        verbose_name_plural = "disques"


class Vinyl(Disk):
    class Meta:
        verbose_name = "vinyle"
        verbose_name_plural = "vinyles"
        ordering = ['title', 'subtitle']


class CD(Disk):
    class Meta:
        verbose_name = "CD"
        verbose_name_plural = "CDs"
        ordering = ['title', 'subtitle']


class Track(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name="titre"
    )

    number = models.PositiveIntegerField(
        verbose_name="numéro",
        blank=True,
        null=True
    )

    disk = models.ForeignKey(
        Disk,
        on_delete=models.CASCADE,
        related_name="tracklist",
        verbose_name="disque"
    )

    def __str__(self):
        if isinstance(self.number, int):
            return f"{self.number}. {self.name}"
        else:
            return self.name

    def str(self):
        return self.__str__()

    class Meta:
        verbose_name = "piste"
        verbose_name_plural = "pistes"
        ordering = ['number', 'name']


class Review(Borrowable):
    number = models.PositiveIntegerField(
        verbose_name="numéro"
    )

    year = models.PositiveIntegerField(
        verbose_name="année",
        null=True,
        blank=True,
        default=None
    )

    month = models.PositiveIntegerField(
        verbose_name="mois",
        null=True,
        blank=True,
        default=None
    )

    day = models.PositiveIntegerField(
        verbose_name="jour",
        null=True,
        blank=True,
        default=None
    )

    def __str__(self):
        return self.title + " n°" + str(self.number)

    class Meta:
        verbose_name = "magazine"
        verbose_name_plural = "magazines"
        ordering = ['title', 'number']


class Game(Borrowable):
    DURATIONS = (
        ('-1h', '-1h'),
        ('1-2h', '1-2h'),
        ('2-3h', '2-3h'),
        ('3-4h', '3-4h'),
        ('4h+', '4h+'),
    )

    duration = models.CharField(
        choices=DURATIONS,
        max_length=255,
        verbose_name="durée"
    )

    players_min = models.IntegerField(
        validators=[MinValueValidator(1)],
        verbose_name="nombre minimum de joueureuses"
    )

    players_max = models.IntegerField(
        validators=[MinValueValidator(1)],
        verbose_name="nombre maximum de joueureuses"
    )

    class Meta:
        verbose_name = "jeu"
        verbose_name_plural = "jeux"
        ordering = ['title']


class Borrow(models.Model):
    borrowable = models.ForeignKey(
        Borrowable,
        on_delete=models.PROTECT,
        verbose_name="objet emprunté",
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        verbose_name="emprunteur",
    )

    borrow_date = models.DateTimeField(
        verbose_name="emprunté le",
    )

    given_back = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name="rendu le",
    )

    borrowed_with = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name="emprunté avec"
    )

    given_back_to = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name="+",
        blank=True,
        null=True,
        verbose_name="rendu à"
    )

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.borrowable.update_present_status()

    def __str__(self):
        return str(self.borrowable) + " " + str(self.user)

    class Meta:
        verbose_name = "emprunt"
        verbose_name_plural = "emprunts"
        ordering = ['-borrow_date']
