from django import forms
from django.forms.models import construct_instance
from django.forms import widgets
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from bdd_med.inputs import Autocomplete, AutocompleteMultiple
from users.models import User
from formset.collection import FormCollection
from formset.widgets import DualSelector, Button, DateInput
from formset.fields import Activator
from formset.dialog import DialogModelForm

from .validators import isbn_validator
from .models import Track, Borrowable, Author, Game, \
    Manga, Comic, Novel, CD, Vinyl, Review


class BorrowForm(forms.Form):
    user = forms.ModelChoiceField(
        queryset=User.objects.all(),
        label="pseudo",
        widget=Autocomplete(User, attrs={'api_url': '/api/users/user/',
                                         'name_field': 'username'}))
    borrowed_items = forms.ModelMultipleChoiceField(
        queryset=Borrowable.objects.filter(present=True, owner="Med"),
        required=False,
        label="médias empruntés",
        widget=AutocompleteMultiple(Borrowable,
                                    attrs={'api_url':
                                           '/api/media/borrowable/',
                                           'api_url_suffix':
                                           '&present=true',
                                           'name_field': 'string_repr'}))


class AuthorDialogForm(DialogModelForm):
    title = "Ajouter un.e auterice"
    epilogue = "Ceci réinitialise le formulaire"
    induce_open = 'borrowable.add_author:active'
    induce_close = '.add:active || .cancel:active'

    add = Activator(
        widget=Button(
            action='submitPartial -> activate("clear") -> reload'))
    cancel = Activator(
        widget=Button(action='activate("clear")'))

    class Meta:
        model = Author
        fields = ['name']

    def is_valid(self):
        if self.partial:
            return super().is_valid()
        self._errors = {}
        return True


class TrackForm(forms.ModelForm):
    id = forms.IntegerField(required=False, widget=forms.HiddenInput)

    class Meta:
        model = Track
        fields = ['id', 'name', 'number']


class TrackCollection(FormCollection):
    min_siblings = 2
    track = TrackForm()
    legend = "Pistes"
    add_label = "Ajouter une piste"
    related_field = "disk"
    # hide_condition = "borrowable.medium_type=='Novel' ||\
    #         borrowable.medium_type=='Manga' ||\
    #         borrowable.medium_type=='Comic' ||\
    #         borrowable.medium_type=='Review' ||\
    #         borrowable.medium_type=='Game'"

    def retrieve_instance(self, data):
        data = data.get('track')
        if data:
            try:
                return self.instance.tracklist.get(id=data.get('id') or 0)
            except (AttributeError, Track.DoesNotExist, ValueError):
                return Track(name=data.get("name"), disk=self.instance)


class CreateBorrowableForm(forms.Form):
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     side_identifier_field = self.fields.get('side_identifier')
    #     side_identifier_field.widget.template_name =\
    #         "media/generate_side_identifier.html"

    medium_type = forms.ChoiceField(
        choices=[('Manga', 'Manga'),
                 ('BD', 'BD'),
                 ('Roman', 'Roman'),
                 ('CD', 'CD'),
                 ('Vinyle', 'Vinyle'),
                 ('Jeu', 'Jeu'),
                 ('Magazine', 'Magazine')],
        required=True,
        label="Type de média",
        initial='Manga',
        widget=forms.RadioSelect())

    title = forms.CharField(required=True, label='Titre')

    subtitle = forms.CharField(required=False, label='Sous-titre')

    authors = forms.ModelMultipleChoiceField(
        Author.objects.all(),
        required=False,
        label="Auteurices",
        widget=DualSelector(
            search_lookup="name__icontains",
            attrs={'df-hide':
                   ".medium_type=='Jeu' || .medium_type=='Magazine'"}))

    add_author = Activator(
        label="Ajouter auteurice",
        widget=Button(
            action='activate',
            attrs={'df-hide':
                   "borrowable.medium_type=='Jeu' || \
                           borrowable.medium_type=='Magazine'"}))

    owner = forms.CharField(required=True, label='Propriétaire')

    isbn = forms.CharField(required=True, label='ISBN',
                           validators=[isbn_validator], min_length=10)

    side_identifier = forms.CharField(
        required=False, label='Cote',
        widget=widgets.TextInput(
            attrs={'df-hide':
                   ".medium_type=='Jeu' || .medium_type=='Magazine'"}))

    generate_side_identifier = Activator(
        label="Générer la cote",
        widget=Button(
            action='submitPartial -> setFieldValue(\
                    borrowable.side_identifier, ^identifier)',
            attrs={'df-hide':
                   "borrowable.medium_type=='Jeu' || \
                           borrowable.medium_type=='Magazine'"}))

    publish_date = forms.DateField(
        required=False, label='Date de publication',
        widget=DateInput(
            attrs={'df-show':
                   ".medium_type=='Roman' || .medium_type=='Manga' || \
                           .medium_type=='BD'"}))

    number = forms.IntegerField(
        required=False, label='Numéro',
        widget=widgets.NumberInput(attrs={'df-show':
                                          ".medium_type=='Magazine'"}))

    year = forms.IntegerField(
        required=False, label='Année',
        widget=widgets.NumberInput(attrs={'df-show':
                                          ".medium_type=='Magazine'"}))

    month = forms.IntegerField(
        required=False, label='Mois',
        widget=widgets.NumberInput(attrs={'df-show':
                                          ".medium_type=='Magazine'"}))

    day = forms.IntegerField(
        required=False, label='Jour',
        widget=widgets.NumberInput(attrs={'df-show':
                                          ".medium_type=='Magazine'"}))

    duration = forms.ChoiceField(
        choices=Game.DURATIONS,
        required=False, label='Durée',
        widget=forms.RadioSelect(attrs={'df-show': ".medium_type=='Jeu'"}))

    players_min = forms.IntegerField(
        required=False,
        label='Nombre min de joueureuses',
        widget=widgets.NumberInput(
            attrs={'df-show': ".medium_type=='Jeu'"}))

    players_max = forms.IntegerField(
        required=False,
        label='Nombre max de joueureuses',
        widget=widgets.NumberInput(
            attrs={'df-show': ".medium_type=='Jeu'"}))

    comment = forms.CharField(required=False, label="Commentaire")

    def is_valid(self):
        # Only validate relevant fields on partial submission
        if self.partial:
            self._errors = {}
            self.cleaned_data = {}

            def clean_field(fieldname):
                bf = self[fieldname]
                field = bf.field
                try:
                    self.cleaned_data[fieldname] = field.clean(bf.data)
                except ValidationError as e:
                    self.add_error(fieldname, e)

            clean_field("title")
            clean_field("subtitle")
            clean_field("authors")
            return not self._errors
        return super().is_valid()


class BorrowableCollection(FormCollection):
    add_author = AuthorDialogForm()
    borrowable = CreateBorrowableForm()
    # tracks = TrackCollection()

    def construct_instance(self):
        assert not self.partial
        form = self.valid_holders["borrowable"]
        model = {'Manga': Manga, 'BD': Comic, 'Roman': Novel, 'CD': CD,
                 'Vinyle': Vinyl, 'Jeu': Game, 'Magazine': Review
                 }.get(form.cleaned_data.get("medium_type"))
        main_object = model()
        instance = construct_instance(form, main_object)
        try:
            instance.full_clean()
            instance.save()
            instance.authors.set(form.cleaned_data["authors"])
            instance.save()
        except (ValidationError, IntegrityError, ValueError) as error:
            self.valid_holders["borrowable"].add_error(None, error)
        return instance


def borrowableform_factory(model_type):
    has_tracklist = hasattr(model_type, "tracklist")
    has_authors = hasattr(model_type, "authors")

    class BorrowableForm(forms.ModelForm):
        if has_authors:
            add_author = Activator(
                label="Ajouter auteurice",
                widget=Button(action='activate'))

        if has_tracklist:
            generate_side_identifier = Activator(
                label="Générer la cote",
                widget=Button(
                    action='submitPartial -> setFieldValue(\
                            borrowable.side_identifier, ^identifier)'))

        class Meta:
            model = model_type
            exclude = ("id", )
            widgets = {"authors":
                       DualSelector(search_lookup="name__icontains"),
                       "publish_date": DateInput,
                       "duration": widgets.RadioSelect(choices=Game.DURATIONS)}

        field_order = ["title", "subtitle", "authors", "add_author", "owner",
                       "isbn", "side_identifier", "generate_side_identifier",
                       "publish_date", "number", "year", "month", "day",
                       "duration", "players_min", "players_max", "comment"]

    class BorrowableCollection(FormCollection):
        borrowable = BorrowableForm()

        if has_authors:
            add_author = AuthorDialogForm()
        if has_tracklist:
            tracklist = TrackCollection()

        def construct_instance(self, main_object):
            assert not self.partial
            form = self.valid_holders["borrowable"]
            instance = construct_instance(form, main_object)
            instance.save()
            if has_authors:
                instance.authors.set(form.cleaned_data["authors"])
            if "tracklist" in self.valid_holders:
                self.valid_holders.get("tracklist").construct_instance(instance)
            return instance

    return BorrowableCollection


class SearchBorrowableForm(forms.Form):
    medium_type = forms.ChoiceField(
        choices=[('Manga', 'Manga'),
                 ('BD', 'BD'),
                 ('Roman', 'Roman'),
                 ('CD', 'CD'),
                 ('Vinyle', 'Vinyle'),
                 ('Jeu', 'Jeu'),
                 ('Magazine', 'Magazine')],
        required=True,
        label="Type de média",
        initial='Manga',
        widget=forms.RadioSelect())

    title = forms.CharField(required=False, label='Titre')

    subtitle = forms.CharField(required=False, label='Sous-titre')

    authors = forms.CharField(
        required=False, label='Auteurice',
        widget=widgets.TextInput(
            attrs={'df-hide':
                   ".medium_type=='Jeu' || .medium_type=='Magazine'"}))

    owner = forms.CharField(required=False, label='Propriétaire')

    isbn = forms.CharField(required=False, label='ISBN')

    side_identifier = forms.CharField(
        required=False, label='Cote',
        widget=widgets.TextInput(
            attrs={'df-hide':
                   ".medium_type=='Jeu' || .medium_type=='Magazine'"}))

    publish_date_gte = forms.DateField(
        required=False, label='Publié après',
        widget=DateInput(
            attrs={'df-show':
                   ".medium_type=='Roman' || .medium_type=='Manga' || \
                           .medium_type=='BD'"}))

    publish_date_lte = forms.DateField(
        required=False, label='Publié avant',
        widget=DateInput(
            attrs={'df-show':
                   ".medium_type=='Roman' || .medium_type=='Manga' || \
                           .medium_type=='BD'"}))

    track = forms.CharField(
        required=False, label="Piste",
        widget=widgets.TextInput(
            attrs={'df-show': ".medium_type=='CD' || \
                   .medium_type=='Vinyle'"}))

    number = forms.IntegerField(
        required=False, label='Numéro',
        widget=widgets.NumberInput(attrs={'df-show':
                                          ".medium_type=='Magazine'"}))

    year = forms.IntegerField(
        required=False, label='Année',
        widget=widgets.NumberInput(attrs={'df-show':
                                          ".medium_type=='Magazine'"}))

    month = forms.IntegerField(
        required=False, label='Mois',
        widget=widgets.NumberInput(attrs={'df-show':
                                          ".medium_type=='Magazine'"}))

    day = forms.IntegerField(
        required=False, label='Jour',
        widget=widgets.NumberInput(attrs={'df-show':
                                          ".medium_type=='Magazine'"}))

    duration = forms.ChoiceField(
        choices=Game.DURATIONS,
        required=False, label='Durée',
        widget=forms.RadioSelect(attrs={'df-show': ".medium_type=='Jeu'"}))

    players = forms.IntegerField(
        required=False,
        label='Jouable à',
        widget=widgets.NumberInput(
            attrs={'df-show': ".medium_type=='Jeu'"}))

    comment = forms.CharField(required=False, label="Commentaire")
