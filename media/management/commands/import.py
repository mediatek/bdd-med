from django.core.management import BaseCommand
from media.models import Author, CD, Comic, Game, Manga, Novel, Review, Vinyl

import json


class Command(BaseCommand):
    help = "Imports the old db"

    def add_arguments(self, parser):
        parser.add_argument('--file', '-f', type=str, default=None,
                            help='dump file')

    # flake8: noqa: C901
    def handle(self, *args, **options):
        path = options["file"]

        with open(path, 'r') as file:
            data = json.loads(file.read())

        all_authors = {}
        all_media = {}
        media_models = [
            'book', 'borrowable', 'cd', 'comic', 'game', 'manga',
            'medium', 'novel', 'review', 'vinyl'
        ]
        media_type = [
            'cd', 'comic', 'game', 'manga', 'novel', 'review', 'vinyl'
        ]

        for media in data:
            model = media['model'].split(".")[1]
            pk = media['pk']
            fields = media['fields']

            if model == "author":
                all_authors[pk] = fields
            elif model in media_models:
                if pk in all_media:
                    all_media[pk].update(fields)
                else:
                    all_media[pk] = fields
                if model in media_type:
                    all_media[pk]['type'] = model

        for _, fields in all_authors.items():
            fields.pop('note')
            Author.objects.get_or_create(**fields)

        for _, fields in all_media.items():
            # removes unnecessary data
            to_remove = [
                'external_url', 'number_of_pages', 'rpm', 'double'
            ]
            ctype = fields.pop('type')
            fields.pop('polymorphic_ctype')
            if 'authors' in fields:
                authors = fields.pop('authors')
            else:
                authors = []
            for key in to_remove:
                if key in fields:
                    fields.pop(key)

            # adds useful data
            fields['owner'] = "med"
            fields['present'] = False
            fields['amount'] = 0
            if 'subtitle' not in fields:
                fields['subtitle'] = ''

            # creates media
            classes = {
                'comic': Comic,
                'manga': Manga,
                'novel': Novel,
                'cd': CD,
                'vinyl': Vinyl,
                'review': Review,
                'game': Game
            }
            m, created = classes[ctype].objects.get_or_create(**fields)

            # add authors
            if not created:
                continue
            for pk_author in authors:
                author = Author.objects.get(
                    name=all_authors[pk_author]['name']
                )
                m.authors.add(author)
