from django.conf import settings
from django.contrib.admin import AdminSite
from django.contrib.sites.admin import Site, SiteAdmin

from users.views import LoginView


class StrongAdminSite(AdminSite):
    def has_permission(self, request):
        """
        Authorize only staff that have the correct permission mask
        """
        # TODO : choisir les permissions
        return request.user.is_superuser

    def login(self, request, extra_context=None):
        return LoginView.as_view()(request)


# Instantiate admin site and register some defaults
admin_site = StrongAdminSite()
admin_site.register(Site, SiteAdmin)
