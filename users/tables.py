from django_tables2 import tables

from .models import User


class UserTable(tables.Table):
    class Meta:
        model = User
        exclude = (
            'id', 'password', 'address', 'is_active', 'email', 'date_joined', 'phone_number',
            'comment', 'is_staff', 'last_name', 'first_name', 'is_superuser', 'last_login'
        )
